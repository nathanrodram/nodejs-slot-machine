"use strict"

var express = require('express'),
    Slot = require('./Slot'),
    app = express();

app.set('views', './views');
app.set('view engine', 'jade');

app.get('/', function(req, res){
    res.render('home', { title: 'Hey', message: 'Hello there!'});
});


app.get('/spin', function(req, res){
    var game = new Slot(),
        shuffled = game.shuffle().slice(0,3),
        result = {};
    result.spin = shuffled;
    result.winner = game.checkWinner(shuffled);
    res.send(result);
});

app.listen(3000);
